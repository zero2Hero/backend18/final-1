package hu.zth.comp;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.CRC32;

public class Duplicate {

    private static final int BUFFER_SIZE = 1048576;

    private Set<Path> paths;
    private long size;
    private long crc;

    private Duplicate(long size, long crc, Set<Path> paths) {
        this.size = size;
        this.crc = crc;
        this.paths = paths;
    }

    public long getSize() {
        return size;
    }

    public Set<Path> getPaths() {
        return paths;
    }

    public long getCrc() {
        return crc;
    }

    public boolean isDuplicate() {
        return paths.size() > 1;
    }

    public long calculateSizeWaste() {
        return size * (paths.size() - 1);
    }

    public static Set<Duplicate> splitByCrc(long size, Set<Path> paths) {
        return createCrcMap(paths).entrySet().stream().
                filter(entry -> entry.getValue().size() > 1).
                map(entry -> new Duplicate(size, entry.getKey(), entry.getValue())).
                collect(Collectors.toSet());
    }
    
    private static Map<Long, Set<Path>> createCrcMap(Set<Path> paths) {
        Map<Long, Set<Path>> crcMap = new HashMap<>();
        paths.forEach(file -> crcMap.computeIfAbsent(calculateCrc(file), k -> new HashSet<>()).add(file));
        return crcMap;
    }

    private static long calculateCrc(Path file) {
        CRC32 crc = new CRC32();
        byte[] buffer = new byte[BUFFER_SIZE];
        try (FileInputStream fis = new FileInputStream(file.toFile())) {
            int read;
            while ((read = fis.read(buffer)) != -1) {
                crc.update(buffer, 0, read);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Could not calculate CRC for file: " +
                    file.toAbsolutePath(), e);
        }
        return crc.getValue();
    }
}

package hu.zth.comp;

public final class Util {
    
    private static final String[] SIZE_UNITS = {"bytes", "KB", "MB", "GB"};
    
    private Util() {
        throw new UnsupportedOperationException();
    }

    public static String getHumanReadableSize(long sizeToConvert) {
        double size = sizeToConvert;
        for (String sizeUnit : SIZE_UNITS) {
            if (size < 1024) {
                return "bytes".equals(sizeUnit) ?
                        String.format("%.0f %s", size, sizeUnit) :
                        String.format("%.3f %s", size, sizeUnit);
            }
            size /= 1024.0d;
        }
        return String.format("%.3f TB", size);
    }
    
    public static long measure(Runnable runnable) {
        long start = System.currentTimeMillis();
        runnable.run();
        return System.currentTimeMillis() - start;
    }
    
}

package hu.zth.comp;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

import static hu.zth.comp.Util.getHumanReadableSize;
import static hu.zth.comp.Util.measure;

public class DuplicateFinder {

    @SuppressWarnings("squid:S106")
    private static final PrintStream OUT = System.out;
    
    @SuppressWarnings("squid:S106")
    private static final PrintStream ERR = System.err;
    
    private Path rootPath;
    private List<Duplicate> duplicates;
    
    public DuplicateFinder(Path rootPath) {
        this.rootPath = rootPath;
    }

    public static void main(String[] args) {
        String rootDirectory = args.length > 0 ? args[0] : System.getProperty("user.dir");
        Path rootPath = Paths.get(rootDirectory);

        if (canRunOnDirectory(rootPath)) {
            DuplicateFinder finder = new DuplicateFinder(rootPath);

            OUT.printf("Searching for duplicates in %s ... ", rootPath);
            OUT.printf("%d ms%n", measure(finder::find));

            List<Duplicate> duplicates = finder.getDuplicates();
            
            duplicates.forEach(DuplicateFinder::printDuplicate);
            OUT.printf("Overall waste: %s%n", 
                    getHumanReadableSize(duplicates.stream().mapToLong(Duplicate::calculateSizeWaste).sum()));
        }
    }

    private static boolean canRunOnDirectory(Path rootPath) {
        if (!rootPath.toFile().isDirectory()) {
            ERR.printf("Target is not a directory or doesn't exists: %s%n", rootPath.toAbsolutePath());
            return false;
        }
        return true;
    }

    private static void printDuplicate(Duplicate duplicate) {
        OUT.println(String.format("Duplicates (crc:%d) with size %s",
                duplicate.getCrc(), getHumanReadableSize(duplicate.getSize())));
        duplicate.getPaths().forEach(path -> OUT.println(" - " + path.toAbsolutePath()));
    }

    private Map<Long, Set<Path>> scanSizeMap() throws IOException {
        Map<Long, Set<Path>> sizeMap = new HashMap<>();
        Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                sizeMap.computeIfAbsent(Files.size(file), k -> new HashSet<>()).add(file);
                return FileVisitResult.CONTINUE;
            }
        });
        return sizeMap;
    }

    public void find() {
        try {
            duplicates = findDuplicates(scanSizeMap());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Duplicate> findDuplicates(Map<Long, Set<Path>> sizeMap) {
        return sizeMap.entrySet().stream().
                filter(entry -> entry.getValue().size() > 1).
                flatMap(entry -> Duplicate.splitByCrc(entry.getKey(), entry.getValue()).stream()).
                filter(Duplicate::isDuplicate).
                collect(Collectors.toList());
    }

    public List<Duplicate> getDuplicates() {
        return duplicates;
    }
}
